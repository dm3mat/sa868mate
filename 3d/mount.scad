pin_height   = 4.5;
pin_travel   = 1.0;

board_width  = 35.6;
board_height = 19.0;
slack        = 0.3;

wall_width   = 4;
overlapp     = 1;
pick_radius  = 10;
drill_dia    = 2;
drill_depth  = 3;

board_depth  = 2*pin_travel;
wall_height  = pin_height-pin_travel+board_depth;
pick_depth   = 2*board_depth;
total_width  = board_width +2*wall_width-2*overlapp;
total_depth  = board_height+2*wall_width-2*overlapp;

translate([-total_width/2, -total_depth/2,0])
difference() {
  color("white")
    cube([board_width+2*(wall_width-overlapp), 
          board_height+2*(wall_width-overlapp),
          wall_height]);
  translate([wall_width-overlapp-slack,
             wall_width-overlapp-slack,
             wall_height-board_depth]) 
    cube([board_width+2*slack, 
          board_height+2*slack, 
          board_depth+slack]);
  translate([wall_width,
             wall_width,
             -slack]) 
    cube([board_width-2*overlapp, 
          board_height-2*overlapp, 
          wall_height+2*slack]);
  
  // Board pickup
  translate([board_width/2+slack+wall_width-overlapp,
             -slack,
             wall_height+pick_radius-pick_depth]) 
    rotate(-90,[1,0,0]) 
      cylinder(board_width+4*slack+wall_width, 
               r=pick_radius);
  
  // Drill holes (short 5x2.5mm self tapper)
  translate([10, 
             wall_width/2,
             -slack])
    cylinder(drill_depth+slack, d=drill_dia);
  translate([10, 
            total_depth-wall_width/2,
             -slack])
    cylinder(drill_depth+slack, d=drill_dia);
  translate([total_width-10, 
             wall_width/2,
             -slack])
    cylinder(drill_depth+slack, d=drill_dia);
  translate([total_width-10, 
             total_depth-wall_width/2,
             -slack])
    cylinder(drill_depth+slack, d=drill_dia);  
}